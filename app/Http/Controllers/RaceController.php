<?php
namespace App\Http\Controllers;

use App\Race;
use App\Circuit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RaceController extends Controller
{
    public function showAllRaces()
    {
        return response()->json(Race::all());
    }

    public function showRace($season, $round)
    {
        /**
         * So we need info from multiple tables in this query:
         * Race Information (Date/Time, Location)
         * Results (All Drivers)
         * Driver information
         * Constructor Information
         * Status (e.g Finished, Retired, DSQ, etc)
         */
        //Get the Race Info via a query
        $race = DB::table('races')->where([
            'year' => $season, 
            'round' => $round
        ])->first();
        //From there we can get the circuit info
        $circuitID = $race->circuitId;
        $circuit = DB::table('circuits')->where([
            'circuitId' => $circuitID
        ])->first();
        $race->Circuit = $circuit;
        $raceID = $race->raceId;
        $results = DB::table('results')->where([
            'raceId' => $raceID
        ])->get();
        //Add Driver Information to the results object
        foreach ($results as $key => $result) {
            $driver = DB::table('drivers')->where([
                'driverId' => $result->driverId
            ])->first();
            $results{$key}->Driver = $driver;
        }
        //Now do the same for Constructor Information
        foreach ($results as $key => $result) {
            $constructor = DB::table('constructors')->where([
                'constructorId' => $result->constructorId
            ])->first();
            $results{$key}->Constructor = $constructor;
        }
        //Now we add Status Info
        foreach ($results as $key => $result) {
            $status = DB::table('status')->where([
                'statusId' => $result->statusId
            ])->first();
            $results{$key}->Status = $status;
        }
        dump($results);
        //return response()->json($race);
    }

}