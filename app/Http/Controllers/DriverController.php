<?php
namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function showAllDrivers()
    {
        return response()->json(Driver::all());
    }

    public function showDriver($ref)
    {
        return response()->json(Driver::where(['driverRef' => $ref])->get());
    }
}