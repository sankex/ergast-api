<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $primaryKey = 'driverRef';
    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $fillable =[
        'driverRef', 'number', 'code', 'forename', 'surname', 'dob','nationality', 'url' 
    ];

    /**
     * The attributes escluded from the model's JSON form
     * 
     * @var array
     */
    protected $hidden = [];
}