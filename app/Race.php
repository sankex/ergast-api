<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $fillable =[
        'raceId','year', 'number', 'round', 'circuitId', 'name','date', 'time', 'url' 
    ];

    /**
     * The attributes escluded from the model's JSON form
     * 
     * @var array
     */
    protected $hidden = [];
}